<?php
// Initialisatiopn de la variable pour appeller l'image à la source
$filename = 'cat.jpg';

// Limite maximal de la larger et de la hauteur
$width = 150;
$height = 150;

// Content type
header('Content-Type: image/jpeg');

// Cacul des nouvelles dimensions
list($width_orig, $height_orig) = getimagesize($filename);

$ratio_orig = $width_orig/$height_orig;

if ($width/$height > $ratio_orig) {
   $width = $height*$ratio_orig;
} else {
   $height = $width/$ratio_orig;
}

// Redimensionnement de l'image
$image_p = imagecreatetruecolor($width, $height);
$image = imagecreatefromjpeg($filename);
imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

// Affichage
imagejpeg($image_p, null, 100);
?>
<?php
// Initialisation des variable et ajout de la watermark
$mark = imagecreatefrompng('mark.png');
$im = imagecreatefromjpeg('omarprop.jpeg');

// Définition des marges (x,y)
$marge_right = 10;
$marge_bottom = 10;
$sx = imagesx($mark);
$sy = imagesy($mark);

// Positionnement par rapport au dimension de l'image
imagecopy($im, $mark, imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom, 0, 0, imagesx($mark), imagesy($mark));

// Affichage de la watermark sur l'image et libération de la mémoire
header('Content-type: image/png');
imagepng($im);
imagedestroy($im);
?>